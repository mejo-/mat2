#!/bin/env python3

# A set of extension that aren't supported, despite matching a supported mimetype
unsupported_extensions = set(['bat', 'c', 'h', 'ksh', 'pl', 'txt', 'asc',
                              'text', 'pot', 'brf', 'srt', 'rdf', 'wsdl',
                              'xpdl', 'xsl', 'xsd'])
